<?php

namespace Tradein\LanguageTranslation\Http;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

const DEFAULT_LANG = 'lang';
const LOCALE_PATH = 'locale/';

class TranslationController extends Controller
{
    protected $translationFilesPath;
    protected $translationFilesPathDefault;
    protected $env;
    protected $keys = [];
    protected $envTypes = ['develop', 'demo', 'production'];

    public function __construct()
    {
        $this->translationFilesPathDefault = resource_path() . '/assets/js/locale/lang';
        $this->env = config('languageTranslation.env');
        $this->translationFilesPath = LOCALE_PATH . $this->env;
    }

    public function index()
    {
        array_splice($this->envTypes, array_search($this->env, $this->envTypes), 1);
        return response()->json([
            'translations' => $this->getAllTranslationsFromFile(),
            'message' => 'translation data',
            'defaultLang' => DEFAULT_LANG,
            'otherEnv' => $this->envTypes,
        ]);
    }

    public function save(Request $request)
    {
        $translations = $request->get('translations');
        if (empty($translations)) {
            return response()->json(['data' => [], 'message' => 'Invalid data']);
        }

        $resFr = $this->saveTranslationToS3('fr', $this->reverseKeyValueFormat($translations['fr'], 'fr'));
        $resEn = $this->saveTranslationToS3('en', $this->reverseKeyValueFormat($translations['en'], 'en'));
        $resDe = $this->saveTranslationToS3('de', $this->reverseKeyValueFormat($translations['de'], 'de'));
        $resEs = $this->saveTranslationToS3('es', $this->reverseKeyValueFormat($translations['es'], 'es'));

        if (!$resEn || !$resFr || !$resDe || !$resEs) {
            return response()->json(['data' => [], 'message' => 'Error saving some data']);
        }

        return response()->json(['data' => 'send updated translation data', 'message' => 'translation saved successfully']);
    }

    public function import(Request $request)
    {
        $environment = $request->get('environment');
        if (!in_array($environment, $this->envTypes)) {
            return response()->json(['data' => [], 'message' => 'Invalid data']);
        }

        $allFiles = Storage::disk('s3')->allFiles(LOCALE_PATH . $environment . '/');
        Storage::disk('s3')->deleteDirectory(LOCALE_PATH . $this->env . '/');
        foreach($allFiles as $file) {
            if (strpos($file, LOCALE_PATH . $environment) > -1) {
                Storage::disk('s3')->copy($file, str_replace($environment, $this->env, $file));
            }
        }

        return response()->json(['data' => 'send updated translation data', 'message' => 'translation imported successfully']);
    }

    protected function getCount()
    {
        //
    }

    protected function getAllTranslationsFromFile()
    {
        $defaultLang = $this->getTranslationFromFile('default');
        return [
            'fr' => $this->keyValueFormat($this->getTranslationFromS3('fr'), $defaultLang),
            'en' => $this->keyValueFormat($this->getTranslationFromS3('en'), $defaultLang),
            'de' => $this->keyValueFormat($this->getTranslationFromS3('de'), $defaultLang),
            'es' => $this->keyValueFormat($this->getTranslationFromS3('es'), $defaultLang),
            'lang' => $this->keyValueFormat($defaultLang, $defaultLang),
        ];
    }

    protected function keyValueFormat($data, $defaultData = [])
    {
        $clearVal = false;
        if (empty($data)) {
            $data = $defaultData;
            $clearVal = true;
        }
        $newData = [];
        foreach($defaultData as $key => $value) {
            $clearVal_1 = false;
            if (!empty($value) && empty($data[$key])) {
                $data[$key] = $value;
                $clearVal_1 = true;
            }
            $tempValue = [];
            foreach($value as $key_1 => $value_1) {
                $tempValue[] = ['prevKey' => $key_1, 'key' => $key_1, 'value' => ($clearVal || $clearVal_1 ? '' : (isset($data[$key][$key_1]) ? $data[$key][$key_1] : '')), 'disabled' => 'disabled'];
            }
            $newData[] = ['key' => $key, 'value' => $tempValue, 'hidden' => true];
        }
        return $newData;
    }

    protected function reverseKeyValueFormat($data, $lang = null)
    {
        $newData = [];
        foreach ($data as $key => $value) {
            $tempValue = [];
            foreach ($value['value'] as $key_1 => $value_1) {
                if ($lang == DEFAULT_LANG) {
                    if ($value_1['key'] != $value_1['prevKey']) {
                        $this->keys[$value_1['prevKey']] = $value_1['key'];
                    }
                } else {
                    if (isset($this->keys[$value_1['key']]) && $value_1['key'] != $this->keys[$value_1['key']]) {
                        $value_1['key'] = $this->keys[$value_1['key']];
                    }
                }
                $tempValue[$value_1['key']] = $value_1['value'];
            }
            $newData[$value['key']] = $tempValue;
        }
        return $newData;
    }

    protected function getTranslationFromFile($lang = DEFAULT_LANG)
    {
        if (file_exists($this->translationFilesPathDefault . "/{$lang}.json")) {
            $translation = file_get_contents($this->translationFilesPathDefault . "/{$lang}.json");
            return json_decode($translation, true);
        }
        return [];
    }

    protected function saveTranslationToFile($lang, $content)
    {
        return file_put_contents($this->translationFilesPath . "/{$lang}.json", json_encode($content));
    }

    protected function getTranslationFromS3($lang = DEFAULT_LANG)
    {
        if (Storage::disk('s3')->has($this->translationFilesPath . "/{$lang}.json")) {
            $res = Storage::disk('s3')->get($this->translationFilesPath . "/{$lang}.json");
            return json_decode($res, true);
        }
        return [];
    }

    protected function saveTranslationToS3($lang, $content)
    {
        return Storage::disk('s3')->put($this->translationFilesPath . "/{$lang}.json", json_encode($content), 'public');
    }
}
